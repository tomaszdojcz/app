<?php

namespace App\Shared\Infrastructure\Uuid;

use Ramsey\Uuid\Uuid as RamseyUuid;

/**
 * This class makes our Domain more independent from the packages from outside of the project.
 * In this example from Ramsey\Uuid\Uuid so we have out own adapted Uuid using RamseyUuid so we can switch package
 * whenever we want and we still have uuid methods covered.
 */
class Uuid extends RamseyUuid
{
    public static function isValueValid($uuid): bool
    {
        return Uuid::isValid($uuid);
    }
}