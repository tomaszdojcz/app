<?php

namespace App\Shared\Infrastructure\Framework;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response extends SymfonyResponse
{
}