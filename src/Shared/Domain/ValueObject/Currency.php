<?php declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class Currency
{
    public function __construct(private string $code)
    {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function __toString(): string
    {
        return "Currency: " . $this->code;
    }
}