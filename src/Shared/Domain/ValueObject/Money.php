<?php declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class Money
{
    public function __construct(private int $amount, Currency $currency)
    {
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }
}