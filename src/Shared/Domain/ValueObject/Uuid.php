<?php declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

use App\Shared\Infrastructure\Uuid\Uuid as SharedUuid;

class Uuid
{
    public function __construct(private string $value)
    {
        $this->isValidUuid($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Uuid $uuid): bool
    {
        return $this->getValue() === $uuid->getValue();
    }

    public function isValidUuid(string $id): void
    {
        if (false === SharedUuid::isValueValid($id)) {
            throw new \Exception(sprintf("Invalid Uuid - %s", $id));
        }
    }

    public function __toString(): string
    {
        return "Uuid: " . $this->getValue();
    }
}
