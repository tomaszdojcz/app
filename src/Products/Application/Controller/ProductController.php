<?php declare(strict_types=1);

namespace App\Products\Application\Controller;

use App\Shared\Infrastructure\Framework\Response;
use App\Products\Infrastructure\Framework\Controller\ProductController as ProductControllerImplementation;

class ProductController implements ControllerInterface
{
    public function __construct(private ProductControllerImplementation $controller)
    {
    }

    public function index(): Response
    {
        return $this->controller->index();
    }

    public function get(int $id): Response
    {
        return $this->controller->get($id);
    }
}