<?php declare(strict_types=1);

namespace App\Products\Application\Controller;

use App\Shared\Infrastructure\Framework\Response;

interface ControllerInterface
{
    public function index(): Response;

    public function get(int $id): Response;
}