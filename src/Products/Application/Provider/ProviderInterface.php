<?php declare(strict_types=1);

namespace App\Products\Application\Provider;

interface ProviderInterface
{
    public function getEntityClassName(): string;
}