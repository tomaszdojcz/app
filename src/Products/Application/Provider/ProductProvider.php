<?php declare(strict_types=1);

namespace App\Products\Application\Provider;

use App\Products\Domain\Entity\Product;
use App\Products\Infrastructure\Persistence\Repository\DoctrineProductRepository;

class ProductProvider implements ProviderInterface
{
    public function __construct(private DoctrineProductRepository $productRepository)
    {
    }

    public function getAll(): array
    {
        return $this->productRepository->getAll();
    }

    public function getOne(int $id): Product
    {
        return $this->productRepository->getOneById($id);
    }

    public function getEntityClassName(): string
    {
        return Product::class;
    }
}