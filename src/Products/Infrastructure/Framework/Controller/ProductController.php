<?php declare(strict_types=1);

namespace App\Products\Infrastructure\Framework\Controller;

use App\Products\Application\Provider\ProductProvider;
use App\Shared\Infrastructure\Framework\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function __construct(private ProductProvider $productProvider)
    {
    }

    /**
     * @Route("/")
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('App\Products\Infrastructure\Framework\View\Product\index.twig.html', [
            'products' => $this->productProvider->getAll()
        ]);
    }

    /**
     * @param int $id
     *
     * @Route("/{id}")
     *
     * @return Response
     */
    public function get(int $id): Response
    {
        return $this->render('App\Products\Infrastructure\Framework\View\Product\product.twig.html', [
            'product' => $this->productProvider->getOne($id)
        ]);
    }
}