<?php

namespace App\Products\Infrastructure\Persistence\Repository;

use App\Products\Domain\Entity\Product;
use App\Products\Domain\Repository\ProductRepositoryInterface;
use App\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

class DoctrineProductRepository extends DoctrineRepository implements ProductRepositoryInterface
{
    public function getAll(): array
    {
        return $this->getRepository(Product::class)->findAll();
    }

    public function getOneById(int $id): Product
    {
        return $this->getRepository(Product::class)->find($id);
    }
}