<?php

namespace App\DataFixtures;

use App\Products\Domain\Entity\Product;
use App\Shared\Domain\ValueObject\Currency;
use App\Shared\Domain\ValueObject\Money;
use App\Shared\Domain\ValueObject\Uuid;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid as RamseyUuid;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 6; $i++) {
            $product = new Product(
                new Uuid(RamseyUuid::uuid4()->toString()),
                'product ' . $i,
                'description ' . $i,
                new Money(++$i, new Currency('PLN'))
            );

            $manager->persist($product);
        }

        $manager->flush();
    }
}
